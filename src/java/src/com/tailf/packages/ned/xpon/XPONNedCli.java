package com.tailf.packages.ned.xpon;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.tailf.conf.ConfBuf;
import com.tailf.conf.ConfException;
import com.tailf.conf.ConfObject;
import com.tailf.conf.ConfPath;
import com.tailf.conf.ConfXMLParam;
import com.tailf.conf.ConfXMLParamValue;
import com.tailf.ned.NedCmd;
import com.tailf.ned.NedErrorCode;
import com.tailf.ned.NedException;
import com.tailf.ned.NedExpectResult;
import com.tailf.ned.NedMux;
import com.tailf.ned.NedWorker;
import com.tailf.ned.SSHSessionException;
import com.tailf.packages.ned.nedcom.CliCommandHelper;
import com.tailf.packages.ned.nedcom.NedComCliBase;
import com.tailf.packages.ned.nedcom.NedCommonLib.NedState;
import com.tailf.packages.ned.nedcom.NedCommonLib.PlatformInfo;
import com.tailf.packages.ned.nedcom.NedSecretCliExt;



/**
 * This class implements NED interface for zte xpon olt-c3|6xx devices
 *
 */

public class XPONNedCli extends NedComCliBase {

  private static final String CONFIG_PROMPT = "\\A\\S*\\(config\\)#";
  private static final String SHOW_RUNNING_CONFIG = "show running-config";

  private enum dt { DEVICE, NETSIM }

  private static final String PROMPT_MODE = "\\A.*\\(.*\\)#";
  private boolean waitForEcho = true;
  private dt devicetype = dt.DEVICE;
  private static final  String  PROMPT = "\\A\\S*#";
  private static final  String ZPROMPT = "\\A[\\S ]+#[ ]?$";
  private static final  Pattern[] PLW;
  private static final  Pattern[] PROMPT_CONFIG;


  private String zteModel;
  private String lastTransformedConfig = null;
  private CliCommandHelper cliCmdHelper;
  private String oltName = null;

  static {
    PLW = new Pattern[] {
        Pattern.compile("\\A.*\\(cfg\\)#"),
        Pattern.compile("\\A.*\\(config\\)#"),
        Pattern.compile(PROMPT_MODE),
        Pattern.compile("\\A\\S.*#")
    };

    PROMPT_CONFIG = new Pattern[] {
        Pattern.compile(CONFIG_PROMPT),
        Pattern.compile(PROMPT_MODE)
    };
  }

  //top nodes (to add when new top node is added in yang)
  private static final String[] PREFIXES = {
      "  ", //mode prefix
      "acl",
      "dhcpv4-l2-relay-agent",
      "dhcpv6-l2-relay-agent",
      "port-identification",
      "pppoe-intermediate-agent",
      "priority-mark",
      "traffic-limit",
      "traffic-profile"
  };

  public XPONNedCli() {
    super();
  }

  /**
   * NED zte-xpon constructor
   * @param device_id       - configured device name
   * @param mux             -
   * @param worker          - worker context
   * @throws Exception
   */
  public XPONNedCli(
      String deviceId,
      NedMux mux,
      boolean trace,
      NedWorker worker)  throws Exception {
    super(deviceId, mux, trace, worker);
    this.cliCmdHelper = new CliCommandHelper(this);
  }


  /**
   * Post constructor callback to setup instance
   *
   * @param worker       - NED Worker
   * @param platformInfo - Platform information (used in NSO platform/ config)
   */
  @Override
  protected void setupInstance(NedWorker worker, PlatformInfo platformInfo) throws Exception {
    if (platformInfo.version.contains("UNKNOWN")) {
      devicetype = dt.NETSIM;
    }
    zteModel = platformInfo.model;
    secrets = new NedSecretCliExt(this);
    secrets.setDebug(true);

  }



  @Override
  public void prepareDry(NedWorker worker, String data)
      throws Exception {
    String[] lines;
    String line;
    String lastline = "";
    String lastorgline = "";
    StringBuilder newdata = new StringBuilder();
    int i;
    logDebug(worker,"PREPARE DRY data:"+data);
    lines = data.split("\n");
    for (i = 0 ; i < lines.length ; i++) {
      lines[i] = lines[i].trim();
      line = modifyLine(worker,lines[i], lastline, lastorgline);
      if (line == null) {
        continue;
      }
      lastline    = line;
      lastorgline = lines[i];
      newdata.append(line+"\n");
    }

    worker.prepareDryResponse(newdata.toString());
  }



  private void moveToTopConfig(NedWorker worker)
      throws IOException, SSHSessionException {
    NedExpectResult res;
    logDebug(worker, "Move to top, if not already there");
    session.println("");
    res = session.expect(PROMPT_CONFIG);
    while(res.getHit() != 0) {
      session.println("exit");
      session.expect("exit", worker);
      res = session.expect(PROMPT_CONFIG);
    }
  }

  private boolean isCliError(String reply) {
    String[] errprompt = {
        "invalid",
        "error",
        "incomplete ",
        "%code " //various error codes
    };

    String[] ignoreprompt = {
        "msvlan: port already in the vlan",
        "msvlan: xcon-vlan rule not existed",
        "gem port does not exist." //deletion of gemport from /pon-onu-mng/gpon-onu* is done automatically
    };

    for (int j = 0; j < ignoreprompt.length; j++) {
      if (reply.toLowerCase().indexOf(ignoreprompt[j]) >= 0) {
          return false;
      }
    }

    int size = errprompt.length;
    for (int n = 0; n < size; n++) {
      if (reply.toLowerCase().indexOf(errprompt[n]) >= 0) {
          return true;
      }
    }
    return false;
  }

  private void print_line_wait_oper(NedWorker worker,String line)
          throws IOException,
          SSHSessionException, ApplyException {
    NedExpectResult res;

    session.println("");
    res = session.expect(new String[]{
        CONFIG_PROMPT,
        ZPROMPT});
    if (res.getHit() == 0) {
      // Send line and wait for echo of line
      session.println("exit");
      session.expect(new String[] { Pattern.quote("exit") }, worker);
    }

    session.println(line);
    session.expect(new String[] { Pattern.quote(line) }, worker);

    // Wait for prompt
    res = session.expect(new String[] { ZPROMPT }, worker);

    // Look for errors
    String[] lines = res.getText().split("\n|\r");
    for(int i = 0 ; i < lines.length ; i++) {
      if (lines[i].toLowerCase().indexOf("error") >= 0 ||
          lines[i].toLowerCase().indexOf("invalid") >= 0) {
        throw new ApplyException(line, lines[i], true, false);
      }
    }
  }

  private boolean print_line_wait(NedWorker worker, String line)
          throws IOException,SSHSessionException, ApplyException {

    NedExpectResult res = null;
    boolean isAtTop = true;
    String[] output;
    final String regex1 = "onu (\\d+) type \\S+ (\\w+) (\\w+)\\s*";
    final String regex2 = "interface gpon-olt_(.*)";


    session.println(line);
    if (waitForEcho) {
      //match the echo or match the prompt+$ in case of line truncated
      session.expect(new String[] { Pattern.quote(line),"$" }, worker);
    }
    res = session.expect(PLW, worker);

    if (res.getHit() == 2) {
      isAtTop = false;
    }

    Pattern p2 = Pattern.compile(regex2);
    Matcher m2 = p2.matcher(line);
    if (m2.find()) {
      oltName = m2.group(1);
      logDebug(worker, "### OLT name:"+oltName);
    }

    output = res.getText().split("\n|\r");
    for (int i = 0 ; i < output.length ; i++) {

      //registration-method handler
      if (output[i].indexOf("%Code 62391-GPONSRV : The entry is existed. This is a re-create operation") >=0) {

        Pattern p1 = Pattern.compile(regex1);
        Matcher m1 = p1.matcher(line);

        if (m1.find()) {
          String onu = m1.group(1);
          String snpw = m1.group(2);
          String snpwVal = m1.group(3);

          logDebug(worker, "### ONU :"+onu);
          logDebug(worker, "### snpw :"+snpw);
          logDebug(worker, "### snpwVal :"+snpwVal);


          //registration method change
          String[] newcmd = {"exit",
          "interface gpon-onu_"+oltName+":"+onu,
          "registration-method "+snpw+" "+snpwVal,
          "exit",
          "interface gpon-olt_"+oltName};

          return sendConfig(worker,newcmd,false);
        }
        else {
          logDebug(worker,"onu regex: "+regex1+" not found on: "+line);
        }
      }

      if (isCliError(output[i])) {
        throw new ExtendedApplyException(line, output[i], isAtTop, true);
      }
    }

    return isAtTop;
  }

  private boolean enterConfig(NedWorker worker, int cmd)
      throws IOException, SSHSessionException {
    NedExpectResult res = null;

    if (devicetype == dt.NETSIM) {
      session.println("config");
    }
    else {
      session.println("config t");
    }
    res = session.expect(PROMPT_CONFIG, worker);
    if (res.getHit() > 2) {
      worker.error(cmd, res.getText());
      return false;
    }
    return true;
  }

  private void exitConfig() throws IOException, SSHSessionException {
    NedExpectResult res;

    if (devicetype == dt.DEVICE) {
      return;
    }

    while(true) {
      session.println("exit");
      res = session.expect(new String[]
          {CONFIG_PROMPT,
          "\\A\\S*\\(cfg\\)#",
          PROMPT_MODE,
          "\\A\\S*\\(cfg.*\\)#",
          PROMPT});
      if (res.getHit() == 4)
        return;
    }
  }

  private String modifyLine(NedWorker worker,String line,String lastline, String lastorgline) {
    /*ned workaround for nso issue related to tailf:cli-no-value-on-delete + tailf:cli-prefix-key: to remove when ENG-23670 is fixed*/
    if (line.indexOf("no security max-mac-learn ") >=0) {
      line = line.replaceAll("no security max-mac-learn (\\d+) vport (\\d+)", "no security max-mac-learn vport $2");
    }
    if (line.indexOf("no max-mac-learn") >=0) {
      line = line.replaceAll("no max-mac-learn (\\d+) vport (\\d+)", "no max-mac-learn vport $2");
    }
    if (devicetype == dt.NETSIM) {
      //netsim only hack as real device needs this behavior
      if (line.indexOf("no schedule tcont ") >=0) {
        line = line.replaceAll("no schedule tcont (\\d+) profile", "no schedule tcont $1");
      }
      return line;
    }
    if (line.equals("!")) {
      return null;
    }
    if (line.equals(lastline)) {
      return null;
    }
    //skip lines that are auto configured by the device
    if (
       // (line.indexOf("no traffic-profile") >= 0 ) ||
        (line.indexOf("no interface gpon-onu") >= 0 ) ||
        (line.indexOf("no pon-onu-mng gpon-onu") >= 0 )||
        (line.indexOf("no interface gpon_onu-") >= 0 )||
        (line.indexOf("no pon-onu-mng gpon_onu-") >= 0 )||
        ((line.indexOf("no port-identification format") >= 0)&&
            (line.indexOf("no port-identification format-profile") < 0))||
        (line.indexOf("no port-location") >= 0 )||
        (line.indexOf("no interface vport-") >= 0 )) {
      return null;
    }


    if (line.indexOf("no pppoe-plus enable vport") >=0) {
      line = line.replaceAll("no pppoe-plus enable vport (\\d+)", "pppoe-plus disable vport $1");
    }
    if (line.indexOf("no dhcp-option82 enable vport") >=0) {
      line = line.replaceAll("no dhcp-option82 enable vport (\\d+)", "dhcp-option82 disable vport $1");
    }
    if (line.indexOf("no pppoe-intermediate-agent enable vport") >=0) {
      line = line.replaceAll("no pppoe-intermediate-agent enable vport (\\d+)", "pppoe-intermediate-agent disable vport $1");
    }
    if (line.indexOf("no dhcpv4-l2-relay-agent enable vport") >=0) {
      line = line.replaceAll("no dhcpv4-l2-relay-agent enable vport (\\d+)", "dhcpv4-l2-relay-agent disable vport $1");
    }

    if (platformInfo.model.indexOf("C3") >=0) {

      //only C300x contains this
      if (line.indexOf("no dhcpv4-l2-relay-agent vlan ") >=0) {
        line = line.replaceAll("no dhcpv4-l2-relay-agent vlan (.*)", "dhcpv4-l2-relay-agent vlan $1 disable");
      }
      if (line.indexOf("no dhcpv6-l2-relay-agent vlan ") >=0) {
        line = line.replaceAll("no dhcpv6-l2-relay-agent vlan (.*)", "dhcpv6-l2-relay-agent vlan $1 disable");
      }
      if (line.indexOf("no dhcpv4-l2-relay-agent") >=0) {
        line = line.replace("no dhcpv4-l2-relay-agent", "dhcpv4-l2-relay-agent disable");
      }
      if (line.indexOf("no dhcpv6-l2-relay-agent") >=0) {
        line = line.replace("no dhcpv6-l2-relay-agent", "dhcpv6-l2-relay-agent disable");
      }
      if (line.indexOf("no pppoe-intermediate-agent vlan") >=0) {
        line = line.replaceAll("no pppoe-intermediate-agent vlan (.*)", "pppoe-intermediate-agent vlan $1 disable");
      }
    }


    //lacp xgei- hack to be able to delete the interface from nso, that is auto deleted by the device
    if (line.indexOf("no xgei-") >=0) {
      line = line.replaceAll("no xgei-(\\d+)/(\\d+)/(\\d+)", "interface xgei-$1/$2/$3\nno smartgroup");
    }

    //encrypted name-en to name
    if (line.indexOf("name-en") >=0) {
      line = line.replace("name-en","name");
    }

    if (line.indexOf("no voip protocol") >=0) {
      line = line.replace("no voip protocol", "voip protocol none");
    }
    if (line.indexOf("no dhcp-ip ethuni") >=0) {
      line = line.replaceAll("no dhcp-ip ethuni (\\w+)/(\\d+)", "dhcp-ip ethuni $1/$2 no-ctrl");
    }
    if (line.indexOf("no lct disable") >=0) {
      line = line.replace("no lct disable", "lct enable");
    }
    if (line.indexOf("no interface eth") >=0) {
      line = line.replaceAll("no interface eth (\\w+)/(\\d+)", "interface eth $1/$2 state unlock");
    }
    if (line.indexOf("no interface wifi") >=0) {
      line = line.replaceAll("no interface wifi (\\w+)/(\\d+)", "interface wifi $1/$2 state unlock");
    }
    if (line.indexOf("no resource-id-assign-mode mode2") >=0) {
      line = line.replace("no resource-id-assign-mode mode2", "resource-id-assign-mode mode1");
    }

    if (line.indexOf("no port-identification sub-option remote-id enable vport") >=0) {
      line = line.replaceAll("no port-identification sub-option remote-id enable vport (\\d+)",
          "port-identification sub-option remote-id disable vport $1");
    }

    if (line.indexOf("no encrypt ") >=0) {
      line = line.replaceAll("no encrypt (\\d+).*", "encrypt $1 disable");
    }

    return line;
  }

  @Override
  public void applyConfig(NedWorker worker, int cmd, String data)
      throws NedException, IOException, SSHSessionException, ApplyException {
    final long start = tick(0);

    String[] lines;
    boolean isAtTop=true;
    lastTransformedConfig = null;

    int fromTh = worker.getFromTransactionId();
    int toTh = worker.getToTransactionId();

    logInfo(worker, "BEGIN APPLY-CONFIG");

    if (!enterConfig(worker, cmd)) {
      // we encountered an error
      return;
    }
    //nedcom secrets parser
    data = parseCLIDiff(worker, data);

    lines = data.split("\n");


    traceInfo(worker, "BEGIN SENDING "+lines.length+" line(s):"+data);
    final long start2 = reportProgress(worker, "sending config...", 0);

    try {
      // Attach to CDB
      maapiAttach(worker, fromTh, toTh);

      isAtTop = sendConfig(worker, lines, isAtTop);

      // make sure we have exited from all submodes
      if (!isAtTop) {
        moveToTopConfig(worker);
      }

      exitConfig();
      // All commands accepted by device, cache secrets, defaults and locks
      try {
          if (secrets.needUpdate()) {
              lastTransformedConfig = getConfig(worker,SHOW_RUNNING_CONFIG);
              secrets.cache(worker, lastTransformedConfig);
          }

      } catch (Exception e) {
          throw new NedException(NedErrorCode.NED_INTERNAL_ERROR,e.getMessage(), e);
      }

      logInfo(worker, "DONE APPLY-CONFIG "+tickToString(start));

    } catch (Exception e) {
      reportProgress(worker, "sending config error", start2);
      throw e;

    } finally {
      maapiDetach(worker, fromTh, toTh);
    }

  }

  /**
   * @param worker
   * @param cmd
   * @param lines
   * @param isAtTop
   * @param lastTime
   * @return
   * @throws IOException
   * @throws SSHSessionException
   * @throws ApplyException
   */
  private boolean sendConfig(NedWorker worker, String[] lines,boolean isAtTop)
      throws IOException, SSHSessionException, ApplyException {
    long time;
    long lastTime = System.currentTimeMillis();

    try {
      String line;
      String lastline = "";
      String lastorgline = "";
      int i;
      for (i=0 ; i < lines.length ; i++) {
        time = System.currentTimeMillis();
        if ((time - lastTime) > (0.8 * writeTimeout)) {
          lastTime = time;
          worker.setTimeout(writeTimeout);
        }
        lines[i] = lines[i].trim();
        line = modifyLine(worker,lines[i], lastline, lastorgline);
        if ( (line == null) || (line.startsWith("!"))) {
          continue;
        }

        // Send line and save lastline to avoid duplicates
        lastline    = line;
        lastorgline = lines[i];
        isAtTop = print_line_wait(worker,line);
        //wait a bit
        waitForTime(worker, line);
      }
    }
    catch (ApplyException e) {
      if (!e.isAtTop) {
        moveToTopConfig(worker);
      }
      if (e.inConfigMode) {
        exitConfig();
      }
      throw e;
    }
    return isAtTop;
  }

  /**
   * wait for ned-settings/zte-xpon/wait-time seconds after a command
   * @param worker
   * @param line
   */
  private void waitForTime(NedWorker worker, String line) {
    final String regex = "onu \\d+ type.*";

    if (Pattern.matches(regex, line)) {

      int waittime;
      try {
        waittime = nedSettings.getInt("wait-time");
      } catch (Exception e1) {
        waittime = 0;
      }
      logDebug(worker, "!!! Sleep for "+waittime +" seconds");
      if (waittime > 0) {
        try {
          Thread.sleep((long)waittime*1000);
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  @SuppressWarnings("serial")
  private class ExtendedApplyException extends ApplyException {
    public ExtendedApplyException(String line, String msg,
        boolean isAtTop,
        boolean inConfigMode) {
      super("command: "+line+": "+msg, isAtTop, inConfigMode);
    }
  }

  @Override
  public void commit(NedWorker worker, int timeout)
      throws Exception {
    if (trace)
      session.setTracer(worker);
    if (devicetype == dt.DEVICE) {
      if (nedSettings.getBoolean("use-write")) {
        print_line_wait_oper(worker, "write");
      }
      else {
        trace(worker, "WRITE command disabled by ned-settings/use-write", "out");
      }
    }
    worker.commitResponse();
  }


  @Override
  public void revert(NedWorker worker, String data)
      throws Exception {

      if (trace)
          session.setTracer(worker);

      this.applyConfig(worker, NedCmd.REVERT_CLI, data);

      if (devicetype == dt.DEVICE) {
        if (nedSettings.getBoolean("use-write")) {
          print_line_wait_oper(worker, "write");
        }
        else {
          trace(worker, "WRITE command disabled by ned-settings/use-write", "out");
        }
      }
      worker.revertResponse();
  }

  @Override
  public void abort(NedWorker worker, String data)
      throws Exception {

    if (trace)
      session.setTracer(worker);

    this.applyConfig(worker, NedCmd.ABORT_CLI, data);
    worker.abortResponse();
  }


  @Override
  public void getTransId(NedWorker worker)
      throws Exception {
    if (trace)
      session.setTracer(worker);

    logDebug(worker,"Calculating config checksum");
    String res;

    // Get configuration
    logDebug(worker,"getTransId :: using device config");
    // Use last cached transformed config from applyConfig() secret code
    if (lastTransformedConfig != null) {
        traceInfo(worker, "Using last transformed config for transaction-id hash");
        res = lastTransformedConfig;
        lastTransformedConfig = null;
    } else {
      res = getConfig(worker,SHOW_RUNNING_CONFIG);
    }

    worker.setTimeout(readTimeout);

    // Calculate MD5 checksum
    byte[] bytes = res.getBytes("UTF-8");
    MessageDigest md = MessageDigest.getInstance("MD5");
    byte[] thedigest = md.digest(bytes);
    BigInteger md5Number = new BigInteger(1, thedigest);
    String md5String = md5Number.toString(16);

    logDebug(worker,device_id+" transid = "+md5String);

    worker.getTransIdResponse(md5String);
  }


  @Override
  protected String getDeviceConfiguration(NedWorker worker) throws Exception {
      return getConfig(worker,SHOW_RUNNING_CONFIG);
  }

  /**
   * @param worker
   * @return
   * @throws SSHSessionException
   * @throws IOException
   * @throws Exception
   */
  private String getConfig(NedWorker worker, String cmd)
      throws IOException, SSHSessionException{
    int i;
    String res;
    this.lastTransformedConfig = null;

    logDebug(worker,"getConfig("+device_id+")");

    session.println(cmd);
    session.expect(cmd);


    if (devicetype == dt.NETSIM) {
      logDebug(worker,"Getting NETSIM config");
      // Get config and reset timeout
      res = session.expect(PROMPT, worker);
      worker.setTimeout(readTimeout);

      i = res.indexOf("No entries found.");
      if (i >= 0) {
        int n = res.indexOf('\n', i);
        res = res.substring(n+1);
      }
      i = res.lastIndexOf("\nend");
      if (i >= 0) {
        res = res.substring(0,i);
      }
      return res;
    }

    StringBuilder buf = extractOutput(ZPROMPT);
    worker.setTimeout(readTimeout);

    res = buf.toString();
    // Strip beginning
    i = res.indexOf("config-version");
    if (i >= 0) {
      int n = res.indexOf('\n', i);
      res = res.substring(n+1);
    }

    // Strip CR
    res = res.replaceAll("\\r", "");

    // Strip ^H
    res = res.replaceAll("\u0008","");
    // Strip ^$
    res = res.replaceAll("\\$\\s*","");
    // Strip allocid
    res = res.replaceAll("\\s+allocid\\s+\\d+\\s+"," ");

    if (zteModel.indexOf("C6") < 0) {
      //concat wrapped lines -> not for C600, that supports long lines
      res = rebuildWrapLines(worker, res);
    }

    /* handling for service-port * vport * other-all tls-vlan * egress/ingress
     * reason: the device is showing the configuration on a single line, but
     * doesn't accepts it at configuration: it needs two lines;
     * splitting the line that it's not accepted by at show NSO into 2 lines
     */
    String regex = "service-port (\\d+) vport (\\d+) other-all "
        + "tls-vlan (\\d+) (\\w+)gress (\\w+)";

    res = res.replaceAll(regex,
        "service-port $1 vport $2 other-all tls-vlan $3\n"
        + "  service-port $1 vport $2 $4gress $5");

    logDebug(worker,"AFTER:"+res);
    return res;
  }

  /**
   * Pre-processing of the device output to concatenate wrapped lines
   * @param worker
   * @param res
   * @return
   */
  private String rebuildWrapLines(NedWorker worker, String res) {
    String[] lines = res.split("\n");
    StringBuilder newConfig = new StringBuilder();
    for (int i = 0; i < lines.length; i++) {
      newConfig.append(lines[i]);
      //special case where device splits lines longer than 80 chars -> rebuild
      //skip this for devices that do support longer lines (e.g c600x)
      if (lines[i].length() != 80) {
        //smaller or longer lines are considered final line -> add \n
        newConfig.append("\n");
      }
      else {
        logDebug(worker, "\n80char LINE DETECTED:"+ lines[i]);

        /*if we are under a mode (line starts with space) check if next line
          starts with space - if not, it's a wrapped line.
         */

        String startOfLine = null;
        for (String prefix : PREFIXES) {
          if (lines[i].startsWith(prefix)) {
            startOfLine = prefix;
            break;
          }
        }
        if (i < lines.length-1) { //not last line
          logDebug(worker, "NEXT LINE: "+ lines[i+1]);

          if(startOfLine == null) {
            logDebug(worker, "LINE NOT MODELED");
          }
          else {
            if (lines[i+1].startsWith(startOfLine)) {
              logDebug(worker, "current and next line start with the "
                  + "same prefix: "+startOfLine+" --> DON'T concat");
              newConfig.append("\n");
            }
            else {
              logDebug(worker, "CONCAT with nextline");
            }
          }
        }
      }
    }
    return newConfig.toString();
  }

  /**
   * @return
   * @throws IOException
   * @throws SSHSessionException
   */
  private StringBuilder extractOutput(String promptS)
      throws IOException, SSHSessionException {
    StringBuilder buf = new StringBuilder();
    //the device hangs when pagination is disabled, so we need to concatenate 512 lines pages
    NedExpectResult output = session.expect(new String[] {"--More--", promptS});
    buf.append(output.getText());
    while (output.getHit() == 0) {
      session.print(" ");
      output = session.expect(new String[] {"--More--", promptS});
      //append the output without the whitespaces that precedes it, e.g: ^H^H^H..
      buf.append("\n").append(output.getText().trim());
    }
    return buf;
  }

  @Override
  public void show(NedWorker worker, String toptag)
      throws Exception {
    String res = "";
    // Only respond to the first toptag
    if (!"interface".equals(toptag)) {
        worker.showCliResponse("");
        return;
    }

    final long start = tick(0);
    if (session != null && trace) {
        session.setTracer(worker);
    }
    final int toTh = worker.getToTransactionId();
    logInfo(worker, "BEGIN SHOW (th="+toTh+")");
    try {
      res = getDeviceConfiguration(worker);
    } finally {
      this.lastTransformedConfig = null;
    }

    if (turboParserEnable &&
        parseAndLoadXMLConfigStream(maapi, worker, schema, res, NedState.SHOW)) {
      res = "";
    }
    else if (robustParserMode) {
      res = filterConfig(res, schema, maapi, worker, null, false).toString();
    }
    logInfo(worker, "DONE SHOW "+tickToString(start));
    worker.showCliResponse(res);
  }



  @Override
  public void command(NedWorker worker, String cmdName, ConfXMLParam[] p)
      throws Exception {
      String cmd  = cmdName;

      if (cmd.equals("show")) {
        if (p.length < 1) {
            worker.error(NedCmd.CMD, "missing argument(s) for subcmd="+cmdName);
        }

        /* Add arguments */
        for (int i = 0; i < p.length; ++i) {
            ConfObject val = p[i].getValue();
            if (val != null)
                cmd = cmd + " " + val.toString();
        }

        if (devicetype == dt.NETSIM) {
            worker.error(NedCmd.CMD, "'"+cmd+"' not supported on NETSIM, "+
                         "use a real device");
            return;
        }


        logDebug(worker,"cmd:"+cmd);
        session.println(cmd);
        session.expect(cmd, worker);

        //get output (with pagination set to 512) and remove backspaces
        String reply = extractOutput(ZPROMPT).toString().replaceAll("\u0008","");

        worker.commandResponse(new ConfXMLParam[] {
                new ConfXMLParamValue("xpon-stats", "result",new ConfBuf(reply))});
      }
      else {
        cliCmdHelper.command(worker, cmdName, p);
      }
  }

  /**
   * Interactor class override
   */

  @SuppressWarnings("deprecation")
  @Override
  public void connectDevice(NedWorker worker) throws Exception {
      connectorConnectDevice(worker);
  }

  @SuppressWarnings("deprecation")
  @Override
  public PlatformInfo setupDevice(NedWorker worker) throws Exception {
      return connectorSetupDevice(worker);
  }


  /**
   * Attach to Maapi
   * @param
   * @throws NedException
   */
  private void maapiAttach(NedWorker worker, int fromTh, int toTh) throws NedException {
      try {
          int usid = worker.getUsid();
          logDebug(worker, "Maapi.Attach: from="+fromTh+" to="+toTh+" usid="+usid);
          if (fromTh != -1) {
              maapi.attach(fromTh, 0, usid);
          }
          if (toTh != -1) {
              maapi.attach(toTh, 0, usid);
          }
      } catch (Exception e) {
          throw new NedException(NedErrorCode.NED_INTERNAL_ERROR, "Internal ERROR: maapiAttach(): "+e.getMessage());
      }
  }


  /**
   * Detach from Maapi
   * @param
   */
  private void maapiDetach(NedWorker worker, int fromTh, int toTh) {
      try {
          logDebug(worker, "Maapi.Detach: from="+fromTh+" to="+toTh);
          if (fromTh != -1) {
              maapi.detach(fromTh);
          }
          if (toTh != -1) {
              maapi.detach(toTh);
          }
      } catch (Exception e) {
          logError(worker, "Internal ERROR: maapiDetach(): "+e.getMessage(), e);
      }
  }

  /*
   **************************************************************************
   * NedSecrets
   **************************************************************************
   */

  /**
   * Used by NedSecrets to check whether a secret is cleartext or encrypted.
   * Method must be implemented by all NED's which use NedSecrets.
   * @param secret - The secret
   * @return True if secret is cleartext, else false
   */
  @Override
  public boolean isClearText(String secret) {
      String trimmed = secret.trim();

      // encrypted
      if (secret.matches("[0-9a-f]{2}(:([0-9a-f]){2})+")) {
          return false;  // aa:11 .. :22:bb
      }
      if (trimmed.contains(" encrypted")) {
          return false;  // XXX encrypted
      }

      // Default to cleartext
      return true;
  }

  /**
  *
  * @param
  * @throws
  */
  // @Override
  @Override
  public void showPartial(NedWorker worker, ConfPath[] paths)
      throws Exception {
    logDebug(worker,"SHOW PARTIAL");
    Set<String> keyPathFilter = keyPathFilter(worker, paths);

    String result = doShowPartial(worker, paths);
    String config = "";
    StringBuilder filteredConfig = filterConfig(result, schema, maapi, worker, keyPathFilter, true);
    result = filteredConfig.toString();
    logDebug(worker, "SHOW_PARTIAL FILTERED("+device_id+")=\n"+result);

    if (turboParserEnable) {
        parseAndLoadXMLConfigStream(maapi, worker, schema, result,NedState.SHOW_PARTIAL);
    } else if (robustParserMode) {
        config = filterConfig(result, schema, maapi, worker, null, false).toString();
    } else {
        config = result;
    }
    worker.showCliResponse(config);
  }

  /**
   * Show partial implemented for specific nodes. Default is the entire config
   * @param worker
   * @param paths
   * @return
   * @throws ConfException
   * @throws SSHSessionException
   * @throws IOException
   * @throws NedException
   * @throws Exception
   */
  private String doShowPartial(NedWorker worker, ConfPath[] paths)
      throws ConfException, IOException, SSHSessionException, NedException {
    StringBuilder result = new StringBuilder();
    for (ConfPath path : paths) {
      ConfObject[] kp = path.getKP();
      String showCmd = SHOW_RUNNING_CONFIG;

      for (int i = 0; i < kp.length; i++) {

        if ((kp[i].toString().equals("xpon:interface")) ||
            (kp[i].toString().equals("xpon:pon-onu-mng"))) {
          //get interface id
          String cmd = "";
          if (kp[i].toString().equals("xpon:interface")) {
            cmd = SHOW_RUNNING_CONFIG + " interface ";
          }  else if (kp[i].toString().equals("xpon:pon-onu-mng")) {
            cmd = "show onu running config ";
          }

          if (i < 2) {
            throw new NedException(NedErrorCode.NED_EXTERNAL_ERROR,"Incomplete command");
          }

          if (kp[i-1].toString().equals("xpon:gpon-onu"))  {
            showCmd = getIfGponCmd(kp[i-2],"gpon-onu_",cmd);
          } else if (kp[i-1].toString().equals("xpon:gpon_onu-"))  {
            showCmd = getIfGponCmd(kp[i-2],"gpon_onu-",cmd);
          } else if (kp[i-1].toString().equals("xpon:gpon_olt-"))  {
            showCmd = getIfGponCmd(kp[i-2],"gpon_olt-",cmd);
          } else if (kp[i-1].toString().equals("xpon:gpon-olt"))  {
            showCmd = getIfGponCmd(kp[i-2],"gpon-olt_",cmd);
          }
        }
      }
      result.append(getConfig(worker,showCmd));
    }
    return result.toString();
  }

  /**
   * get interface gpon-_olt_-,  gpon-_onu_-
   * @param kp
   * @param ifname
   * @param cmd
   * @return
   */
  private String getIfGponCmd(ConfObject kp, String ifname, String cmd) {
    String showCmd;
    String[] keys = kp.toString().replace("{", "").replace("}", "").split(" ");

    showCmd = cmd+ifname+keys[0]+"/"+keys[1]+"/"+keys[2];
    if(ifname.contains("onu")) {
      showCmd +=":"+keys[3];
    }
    return showCmd;
  }
}
