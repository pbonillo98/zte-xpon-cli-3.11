// =========================================================================
//                          ZTE-XPON NED
// =========================================================================

module tailf-ned-zte-xpon-stats {
  namespace 'http://tail-f.com/ned/zte-xpon-stats';
  prefix xpon-stats;

  import tailf-common {
    prefix tailf;
  }

  // This implementation of RPC actions is designed to work with
  // NEDCOM implementation of command() method to support a generic
  // mechanism to send any kind of command to any NED, as long as the
  // current yang RPC model is kept. For details, please check Confluence
  // NEDCOM documentation

  container EXEC {

    tailf:action nonconfig-actions {
      tailf:actionpoint ncsinternal {
        tailf:internal;
      }
      input {
        list action {
          tailf:info "a list of actions to be executed on the device";
          tailf:cli-suppress-mode;
          key action-payload;
          leaf action-payload {
            type string;
          }

          list interaction {
            tailf:cli-suppress-mode;
            key prompt-pattern;
            leaf prompt-pattern {
              type string;
            }
            leaf value {
              type string;
            }
          }

          leaf internal {
            type empty;
          }
        }
      }

      output {
        leaf result {
          type string;
        }
      }
    }

    // "generic" show command
    tailf:action "show" {
      tailf:info "Execute show commands";
      tailf:actionpoint "ncsinternal" {
        tailf:internal;
      }
      input {
        leaf-list args {
          tailf:cli-drop-node-name;
          tailf:cli-flat-list-syntax;
          type string {
            tailf:info "show argument(s)";
          }
        }
      }
      output {
        leaf result {
          type string;
        }
      }
    }
  }
}
