zte-xpon NED README
--------------------

1. Introduction
---------------

   This document describes - zte-xpon - the NED for ZTE C300x & C600x
   devices.

   The NED connects to the device CLI using either SSH or
   Telnet.

   Configuration is done by sending native CLI commands in a
   transaction to the device through the communication channel. If a
   single command fails, the whole transaction is aborted and reverted.

   If you suspect a bug in the NED, please enable raw tracing and
   re-run the failing transaction, starting with a sync-from. Finally,
   file a ticket and attach the raw trace.


2. Getting started
------------------

   Set up the environment in $DIR, change DIR to something suitable

     > tar xvzf ned-zte-xpon.tgz
     > mv ned-zte-xpon $NCS_DIR/packages/neds/

     > export DIR=/tmp/ncs-zte-xpon
     > ncs-setup --ned-package $NCS_DIR/packages/neds/zte-xpon --dest $DIR
     > cd $DIR

   Start ncs:
     > ncs

   Start NCS CLI:
     > ncs_cli -C -u admin


3. Configure device in NCS
--------------------------

   In the NCS CLI configure the device. Enter configuration mode:
     # configure

   Set the required configurables, address, remote user data including
   remote secondary password, if needed:
     # devices device ztedev address <device address>
     # devices device ztedev device-type cli ned-id zte-xpon
     # devices device ztedev state admin-state unlocked

   Set port and protocol:
     # devices device ztedev port <typically 22 or 23>
     # devices device ztedev device-type cli protocol <telnet or ssh>

   Set authgroup:
     # devices authgroups group auth-groupname umap admin remote-name
     <login username>
     # devices authgroups group auth-groupname umap admin remote-password
      <login password>
     # devices device ztedev authgroup auth-groupname
     # commit

   Enable debug and raw trace:

     Make the NED dump all CLI messages sent to/from the ZTE device
     # devices global-settings trace raw
     # commit

     Make the NED print debug log messages
     # java-vm java-logging logger com.tailf.packages.ned.xpon level level-debug
     # commit

   Exit configure mode:
     # exit

   Try to connect to the device:
     # devices device ztedev connect

   Read the configuration from the device:
     # devices device ztedev sync-from

   Look at the configuration:
     # show running-config devices device ztedev

     ! For TELNET connection, the echo must be enabled (disabled by default):
     # ned-settings zte-xpon connection terminal server-echo true



4. Sample Device Configuration
------------------------------

 #devices device ztedev config

     interface gpon-olt_1/2/15
      onu 1 type ZTE-F601 pw AAA0000010
    exit
    interface gpon-onu_1/2/15:1
       dba mode sr
       tcont 1 profile TC_type3_500M_25M
       tcont 2 profile TC_type1_512k
       gemport 1 tcont 1 queue 1
       gemport 2 tcont 2 queue 2
       sn-bind enable sn
    exit
    pon-onu-mng gpon-onu_1/2/15:1
    service cos1 gemport 1 cos 1 vlan 935
    service cos5 gemport 2 cos 5 vlan 837
    vlan port eth_0/1 mode trunk
    vlan port eth_0/1 vlan 935,837
    vlan port eth_0/1 translate vlan 935 svlan 935 svlan-pri 1
    vlan port eth_0/1 translate vlan 837 svlan 837 svlan-pri 5
    exit

   See what you are about to commit:

   # commit dry-run outformat native
native {
    device {
        name ztedev
        data interface gpon-olt_1/2/15
             onu 1 type ZTE-F601 pw AAA0000010
             exit
             !
             interface gpon-onu_1/2/15:1
             sn-bind enable sn
             tcont 1 profile TC_type3_500M_25M
             gemport 1 tcont 1 queue 1
             tcont 2 profile TC_type1_512k
             gemport 2 tcont 2 queue 2
             dba mode sr
             exit
             !
             pon-onu-mng gpon-onu_1/2/15:1
             service cos1 gemport 1 cos 1 vlan 935
             service cos5 gemport 2 cos 5 vlan 837
             vlan port eth_0/1 mode trunk
             vlan port eth_0/1 vlan 837,935
             vlan port eth_0/1 translate vlan 837 svlan 837 svlan-pri 5
             vlan port eth_0/1 translate vlan 935 svlan 935 svlan-pri 1
             exit
             !
    }
}


   Commit new configuration in a transaction:

   # commit
   Commit complete.

   Verify that NCS is in-sync with the device:

    # devices device ztedev check-sync
    result in-sync

   Compare configuration between device and NCS:

   # devices device ztedev compare-config
   #

   Note: if no diff is shown, supported config is the same in NCS as on the device


5. How to avoid out-of-sync
------------------------------
   The ZTE device is very dynamic and does various automatic changes to
   the running configuration.

   E.g:
   Deleting a onu interface from gpon-olt :

            interface gpon-olt_1/2/15
              no onu 1
            exit

   The device will also delete the gpon-onu_1/2/15:1 interface and the
   pon-onu-mng/gpon-onu_1/2/15:1 interface

   To be in sync with the device, the user must mimic its behavior. In this case,
   it has to send the following commands that will take care of the deletion
   inside NCS database. These commands will not be sent to the device by the
   ned:

        no interface gpon-onu_1/2/15:1
        no pon-onu-mng gpon-onu_1/2/15:1


6. RPC Actions
-------------------------
6.1 SHOW commands with paginated output
-------------------------------------------------------------
  To get live SHOW command output the 'live-status EXEC show'
  command is used. The command supports multiple string parameters that will
  be passed to the device in the same format. This command supports paginated
  output and exposes the entire result.

  E.g:

  admin@ncs(config)# devices device live-status EXEC show running-config "|"
  include tcont

result | include tcont
  profile tcont TC_type1_512k type 1 fixed 512
  profile tcont TC_type3_100M_10M type 3 assured 10240 maximum 102400
  profile tcont TC_type3_100M_13M type 3 assured 13312 maximum 102400
  profile tcont TC_type3_100M_15M type 3 assur
  ....


admin@ncs(config)# devices device live-status EXEC show traffic-profile result
-----------------------------------------------------------------
No. 1 profile : 10M
-----------------------------------------------------------------
No. 2 profile : 12M
-----------------------------------------------------------------
No. 3 profile : 48M
-----------------------------------------------------------------
No. 4 profile : 50M
-----------------------------------------------------------------
No. 5 profile : 100M
....

6.2 Sending interactive RPC ations
-------------------------

    devices device <devName> live-status EXEC nonconfig-actions action
    { action-payload "<command name and parameters>" }

   The RPC actions are categorized as:
   - simple commands that does not use additional prompts (eg "show version")
   - interactive commands that uses additional prompts (eg a RPC's that requests
    username/password or any other prompts)
   - internal NED commands, that does not interact with the device but with the
   NED.

   The last category is supported but not implemented for a specific feature.

   For this generic RPC to work, the expected patterns must be defined in:
   ned-settings/rpc-actions/expect-patterns: a list of regex to define additional
   prompt patterns.

   For zte-xpon ned:
      ned-settings zte-xpon rpc-actions expect-patterns "\A.*\(.*\)#"
      ned-settings zte-xpon rpc-actions expect-patterns "\A[a-zA-Z0-9][^\# ]+#[ ]?$"



   Simple command format is as follows:

   action { action-payload "RPC CLI command" }

   Interactive command contains the simple command and adds the following list:

   action { action-payload "import bw-list bl1 use-mgmt-port
   scp://11.11.11.11/file" interaction { prompt-pattern
   "User name.*" value myuser } interaction { prompt-pattern Password.*
   value mypassword } interaction
   { prompt-pattern "Do you want to overwrite.*" value yes } interaction {
   prompt-pattern \"Do you want to save the remote host information.*\"
   value no } }

   In the above command, the interaction list defines each prompt that it is
   expected from the device, along with its corresponding value.
   Note that prompt-pattern is compiled in a regular expression, so special
   characters that are expected from prompts should be escaped.
   The order of the interaction list definition is not important, but all the
   possible expected prompts should be defined.
   For example, in the above command, the prompt "Do you want to
   overwrite.*" is only active when the file exists.

   Internal commands looks the same as simple commands, but also contain the
    keyword "internal":

   action { action-payload "Internal RPC" internal }

   All the above command sub-categories can be chained and sent in the same
   request, by defining a list of actions.

   Any device commands can be sent using the RPC actions. E.g: configuring
   "default" entries for ip, gateway, vlan, or exec commands like:
   "set hitless-restart enable" and "reload"

   Example for /pon-onu-mng/gpon-onu*/reboot:
   live-status EXEC nonconfig-actions action { action-payload "config terminal" }
   action { action-payload "pon-onu-mng gpon-onu_1/3/1:7" }
   action { action-payload reboot interaction { prompt-pattern ".*\[yes/no\].*"
   value "yes" } }


7. NED settings
-------------------------
The zte-xpon NED supports the following custom ned-settings:

  7.1 use-write
  --------------------
'devices device <deviceName> ned-settings zte-xpon use-write' :

  -  set to True to send 'WRITE' command at commit (Default)
  -  set to False to skip sending 'WRITE' command

  7.2 wait-time
  ------------------------
  Some devices need more time to process some commands, before
  accepting others.
  E.g:  interface gpon-olt_*/onu */type *
  The  ned-settings zte-xpon wait-time is the value in seconds to wait after the
  above command was sent to the device.


8. Special cases - dynamic device behavior
---------------------------------------------------------------
  8.1 Element deletion done by setting an element to a different value
  ----------------------------------------------------------------------------------------------------------

  Taking as exampe "/pppoe-intermediate-agent":

  To delete a entry in 'pppoe-intermediate-agent vlan *', the device does't accept
  a "no" command on pppoe-intermediate-agent but it expects a 'disable' word
  to be sent after the vlan value:
    pppoe-intermediate-agent vlan 1000 disable
  To be able to delete an entry, the NED will accept a 'no' command on this
  element, but it will sent to the device the accepted version:
  "no pppoe-intermediate-agent vlan 1000" -> "pppoe-intermediate-agent vlan
  1000 disable"

  List of elements where a NSO NO command leads to a modify command that
  actually deletes the element(s):

     NSO command -> device command
     -----------------------------------------------------
   - no pppoe-intermediate-agent vlan * -> pppoe-intermediate-agent vlan *
   disable

   - no dhcpv4-l2-relay-agent vlan * -> dhcpv4-l2-relay-agent vlan * disable

   - no dhcpv6-l2-relay-agent vlan * -> dhcpv6-l2-relay-agent vlan * disable

   - no dhcpv4-l2-relay-agent -> dhcpv4-l2-relay-agent disable

   - no dhcpv6-l2-relay-agent -> dhcpv6-l2-relay-agent disable

   - no pppoe-plus enable vport * -> pppoe-plus disable vport *

   - no dhcp-option82 enable vport * -> dhcp-option82 disable vport *

   - no pppoe-intermediate-agent enable vport * -> pppoe-intermediate-agent
   disable vport *

   - no dhcpv4-l2-relay-agent enable vport * -> dhcpv4-l2-relay-agent disable
   vport *

   - no voip protocol -> voip protocol none

   - no dhcp-ip ethuni * -> dhcp-ip ethuni * no-ctrl

   - no lct disable -> lct enable

   - no interface eth *  -> interface eth * state unlock

   - no interface wifi * -> interface wifi * state unlock

   - no resource-id-assign-mode mode2 -> resource-id-assign-mode mode1

   - no port-identification sub-option remote-id enable vport * ->
   port-identification sub-option remote-id disable vport *

Note: To avoid out-of-sync errors, the values that are leading to element
deletion are not available to be configured from the user interface
(not modeled in yang). E.g: "interface wifi * state unlock" -> unlock not available.
Only by element deletion (no command) these values can be actually set.

  8.2 C600: lacp interface xgei-* smartgroup *
  -----------------------------------------------------------------------
  When a smartgroup is deleted under /lacp/interface xgei-*/, the device also
  deletes the interface xgei. However, the device doesn't accept deletion of the
  interface xgei by the user ("no" command not available). This causes an
  out-of-sync issue. To avoid it, the ned allows the deletion of the interface xgei
  and transforms it to the correct sequence to the device. The user must mimic
  the device behavior, so this sequence must be used:

  admin@ncs(config-config)# lacp
admin@ncs(config-lacp)# no interface xgei-1/18/3
admin@ncs(config-lacp)# commit dry-run outformat native
native {
    device {
        name z600
        data lacp
             interface xgei-1/18/3
             no smartgroup
    }
}

  8.3 C600: vlan */ cos/copy-to-[inner|outer] deletion
  -----------------------------------------------------------------------
  Device behavior:
    The vlan list entry is automatically deleted by the device when
  cos copy-to-inner is deleted.
  Solution:
    To avoid out-of-sync, the user/service must delete the the list entry and not
  cos copy-to-inner.

  E.g: show run:

  vlan 104
  cos copy-to-inner ingress net-side

  Out-of-sync case:
  vlan 104
    no cos copy-to-inner

  In sync-case:
  no vlan 104

  The behavior on the device side is the same for both cases: the list entry will
  be automatically deleted .

  Note: this is true when cos copy-to-inner is the only element in the vlan list.

  8.4 Encrypted elements
  --------------------------------------
  Some elements, for some device and software versions are getting encrypted
  by the device and appear as "<element-name>-en" in running config.
  One example is /interface gpon-onu*/name & name-en.

  As the NED is a super-set of multiple device versions and software versions,
  both "name" and "name-en" are available in the yang model.
  If the current device is using encryption, only "name-en" should be configured.
  The ned will know about the encryption and will not generate out-of-sync error
  messages.
  The non-encrypted version (e.g: name) should be used only by the devices that
  do NOT encrypt its value.

  8.5 Changing the ONU registration method
  ------------------------------------------------------------------
  To change the registration method for a onu entry the interface gpon-onu_a/b/c:x
  registration-method <type> <value> is used, by the device. This command
  actually changes interface gpon-olt_a/b/c onu x sn|pw values. Adding it to the
  yang model causes an out-of-sync as it doesn't hold value, but it changes another
  part of the yang model.
  Trying to modify an existing onu entry under interface gpon-olt_a/b/c it will
  cause the following error: "%Code 62391-GPONSRV : The entry is existed.
  This is a re-create operation"

  In order to keep the sync, but also to be able to change the registration
  method, the NED allows modifying it, the nso regular way (the same as creating it),
  and when the error is reported by the device, it actually sends the correct
  command to the device:
  interface gpon-onu_a/b/c:x registration-method <type> <value>

  E.g:
  show running-config :
        interface gpon-olt_1/1/2
           onu 1 type ZTE-F601 pw cisco1

  changing the type and value:
        interface gpon-olt_1/1/2
           onu 1 type ZTE-F601 sn TEST12345678

  what the Ned will actually send (not visible in dry-run native):
      interface gpon-onu_1/1/2:1
         registration-method sn TEST12345678

  This way, what is sent is the same thing with that is applied, even if through a
  different set of commands, keeping all in sync.

   Note: As this behavior can be triggered only by the error message reported
   by the device, that is sent only at commit time,  the commit dry-run
   outformat native will NOT show what is actually  sent to the device
    (registration-method command).

9. Show partial
----------------------
Show partial behaviour is implemented with device specific commands for
only several elements:

interface gpon-olt  using "show running-config interface gpon-olt*"

interface gpon-onu using "show running-config interface gpon-onu*"

pon-onu-mng gpon-onu using "show onu running config gpon-onu*"



E.g:
NSO:
admin@ncs(config)# devices partial-sync-from path [
 /devices/device[name='zte-1']/config/xpon:interface/gpon-onu[id='1'][slot='1'][port='3'][onu-number='1']
  /devices/device[name='zte-1']/config/xpon:pon-onu-mng/gpon-onu[id='1'][slot='1'][port='3'][onu-number='1']
  /devices/device[name='zte-1']/config/xpon:interface/gpon-olt[id='1'][slot='1'][port='3'] ]

Device:
show running-config interface gpon-olt_1/1/3
show running-config interface gpon-onu_1/1/3:1
show onu running config gpon-onu_1/1/3:1

LOGS:
- SHOW_PARTIAL FILTERED(zte-1)=

interface gpon-olt_1/1/3
  shutdown
  linktrap disable
  onu 1 type ZTE-F601 pw cisco1
  onu 2 type ZTE-F601 sn TEST12345678
!

interface gpon-onu_1/1/3:1
!

pon-onu-mng gpon-onu_1/1/3:1
!


Except the above all other elements are using "show running-config"

For all partial shows, the NED will filter the config and will load into NSO cdb
only the requested output (even if the device command is show running-config,
that displays all config due to lack of a device command to show only specific
elements, the NED will load only the requested output).
